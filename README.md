# Terraform and Ansible example to boot following:

## Things we already have (assuming)
> VPC ID: Given

> Subnet IDs:  Given (Private and Public)

> Username : Given Will tag all resources with this username

> KeyPair : Given Will boot instance with this keypair

> AWS Region : Given

> Subnet CIDR : Given

> VPC CIDR : Given

## Keythings about VPC architecture

* Private Subnet can be reached from Public Subnet (Route tables)
* Public Subnet can reach Internet (Route table via IG)
* Private Subnet can reach Internet using Nat Gateways in Public Subnets ( Route tables )

## Action of Code will be as follow
* Launch a bastion instance in public subnet
* Launch a webserver instance in public subnet
* Launch a dbserver instance in private subnet
* Attach Security Groups to all above instances as follow
  * Bastion can be reached on 22 port from world
  * Webserver can be reached on 80 and 443 from world and 22 from Public Subnet CIDR
  * DB instance can be reached on port 80, 443 and 9200 from Public Subnet CIDR
* Ansible scripts will provision both webserver and db with Nginx and Elastic Search respectively
> Note : Nginx is a proxy to Elastic Search just to test if webserver is able to connect Elastic Search or not.

## Results
Elastic search can be directly accessed from webserver via Nginx try hitting port 80 of webserver and you will directly see response of Elastic Search

# Second part (Auto Scaling with terraform)
## Things we already have (assuming)
> A feedback mechanism is available with instances which can hit an HTTP endpoint with count of instances required

## Implementation
> An application server in Flask, which takes an argument count of webservers

> Using Jinja templates add a override.tf with webserver count

> Initiate terraform apply