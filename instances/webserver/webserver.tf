provider "aws" {
    region = "${var.aws_region}"
}

resource "aws_security_group" "webserver" {
    name            = "webserver"
    description     = "Allow traffic to pass from the internet to public subnet"

    ingress {
        from_port   = 80
        to_port     = 80
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port   = 443
        to_port     = 443
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["${var.public_subnet_cidr}"]
    }
    ingress {
        from_port   = -1
        to_port     = -1
        protocol    = "icmp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }

    vpc_id          = "${var.vpc_id}"
    tags {
        Name        = "WebServer"
        Enviornment = "${var.env}"
		CreatedBy   = "${var.user_name}"
    }
}

resource "aws_instance" "webserver" {
    count                       = "${var.count}"
    ami                         = "${var.ami}"
    instance_type               = "${var.instance_size}"
    key_name                    = "${var.aws_key_name}"
    vpc_security_group_ids      = ["${aws_security_group.webserver.id}"]
    subnet_id                   = "${var.public_subnet_id}"
    associate_public_ip_address = true

    tags {
        Name        = "Webserver Instance ${count.index}"
		Enviornment = "${var.env}"
		CreatedBy   = "${var.user_name}"
    }
}

output "webserver_ips" {
    value = ["${aws_instance.webserver.*.private_ip}"]
}