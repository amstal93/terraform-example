from flask import Flask
import subprocess
from jinja2 import Template
app = Flask(__name__)

@app.route("/")
def index():
	return "Auto Scale Service is UP"


@app.route("/scaleup/webserver/<count>")
def scale_up(count):
	override_template_file = open('./scale_variables.tf.j2').read()
	override_template = Template(override_template_file)
	config = override_template.render(web_server_count=count)
	with open('./override.tf', 'w') as cfg:
		cfg.write(config)
	subprocess.Popen(["/usr/local/bin/terraform", "apply"])
	return "OK"

@app.route("/scaledown/webserver/<count>")
def scale_down(count):
	override_template_file = open('./scale_variables.tf.j2').read()
	override_template = Template(override_template_file)
	config = override_template.render(web_server_count=count)
	with open('./override.tf', 'w') as cfg:
		cfg.write(config)	
	subprocess.Popen(["/usr/local/bin/terraform", "apply"])
	return "OK"

if __name__ == '__main__':
    app.debug=True
    app.run(host='0.0.0.0', port=8085)